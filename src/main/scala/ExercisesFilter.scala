class ExercisesFilter:
  // TODO: Use the filter function of List to transform list l into a list of only even numbers
  def filterEven(l: List[Int]): List[Int] = ???

  // TODO: Use the filter function of List to transform list l into a list of only string starting with "start"
  def filterStart(l: List[String], start: String): List[String] = ???

  // TODO: Use the filter function of List to transform list l into a list of only string with size smaller than "size"
  def filterSize(l: List[String], size: Int): List[String] = ???
  
  // TODO: Use the filter function of List to transform list l into a list of only Tuples which thi int part in greater than "min" and the string patr contains "s"
  def filterTuple(l: List[(Int, String)], min: Int, s: String): List[String] = ???
